<?php
namespace App\Controller;

use App\Entity\Coffee;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class CoffeeChoiceController extends AbstractController
{
    public function index(Request $request)
    {
        $coffee = new Coffee();

        if ($request->isMethod('POST')) {
            return $this->render('coffee/selected.html.twig', [
                'data' => $request->request->get('form') ?? []
            ]);
        }
        $form = $this->createFormBuilder($coffee)
            ->add('ownerName', TextType::class, ['label' => 'Enter your name please'])
            ->add('saturation', ChoiceType::class,  [
                'choices' => [
                    'Harmonious' => 'Harmonious',
                    'Complicated' => 'Complicated',
                    'Rich' => 'Rich',
                ]
            ])
            ->add('sour', ChoiceType::class,  [
                'choices' => [
                    'Semi-sweet' => 'Semi-sweet',
                    'Normal' => 'Normal',
                    'Bitter' => 'Bitter',
                ]
            ])
            ->add('roasting', ChoiceType::class,  [
                'choices' => [
                    'super' => 'super',
                    'medium' => 'medium',
                    'small' => 'small',
                ]
            ])
            ->add('Select', SubmitType::class)
            ->getForm();

        return $this->render('coffee/new.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
