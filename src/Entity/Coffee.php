<?php
namespace App\Entity;

class Coffee
{
    protected $saturation;
    protected $roasting;
    protected $sour;
    protected $ownerName;

    /**
     * @return mixed
     */
    public function getOwnerName()
    {
        return $this->ownerName;
    }

    /**
     * @param mixed $ownerName
     */
    public function setOwnerName($ownerName)
    {
        $this->ownerName = $ownerName;
    }

    /**
     * @return mixed
     */
    public function getSaturation()
    {
        return $this->saturation;
    }

    /**
     * @param mixed $saturation
     */
    public function setSaturation($saturation)
    {
        $this->saturation = $saturation;
    }

    /**
     * @return mixed
     */
    public function getRoasting()
    {
        return $this->roasting;
    }

    /**
     * @param mixed $roasting
     */
    public function setRoasting($roasting)
    {
        $this->roasting = $roasting;
    }

    /**
     * @return mixed
     */
    public function getSour()
    {
        return $this->sour;
    }

    /**
     * @param mixed $sour
     */
    public function setSour($sour)
    {
        $this->sour = $sour;
    }
}
